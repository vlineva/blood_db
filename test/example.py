# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 11:51:34 2021

@author: user94
"""

import sys
from pathlib import Path
import pandas as pd
sys.path.append(str(Path(__file__).resolve().parent.parent))
from upload import DataUploader
from get_data import DataDownloader

#в файле db_config.json нужно прописать путь до базы данных и initial_table
#"your/path/to/blood_db/blood_db.db" & "your/path/to/blood_db/initial_table.tsv"

# example 1: db creation and filling
data_uploader = DataUploader()
example_1 = pd.read_csv(str(Path(__file__).resolve().parent / 'example_1.tsv'), sep = '\t')
data_uploader.add_data(example_1)

# example 2: some data addition
example_2 = pd.read_csv(str(Path(__file__).resolve().parent / 'example_2.tsv'), sep = '\t')
data_uploader.add_data(example_2)

# example 3: download some data from db: filtration by classes
data_downloader = DataDownloader()
field_1 = 'Run'
names_1 = example_2['Run'].values.tolist()
result_1 = data_downloader.get_annotation(field = field_1, names = names_1)
print(f'result_1 : \n {result_1}')

#example 4: download some data from db: filtration by attributes
field_2 = 'QC_score'
names_2 = example_2['QC_score'].values.tolist()
result_2 = data_downloader.get_annotation(field = field_2, names = names_2)
print(f'result_2 : \n {result_2}')
 