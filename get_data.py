# -*- coding: utf-8 -*-
"""
Created on Thu Dec 24 13:51:50 2020

@author: user94
"""

# from sqlalchemy import create_engine
# from sqlalchemy.orm import sessionmaker
import sys
from pathlib import Path
sys.path.append(str(Path(__file__).resolve().parent.parent))
import pandas as pd 
from sqlalchemy import or_
from collections import defaultdict 
import copy 
from sqlalchemy import select
from schema import initialization


# db_config_path = Path(__file__).resolve().parent / 'db_config.json'
# db_dict, engine, Base, classes, inspector = initialization(db_config_path)

class DataDownloader():
    def __init__(self):
        db_config_path = Path(__file__).resolve().parent / 'db_config.json'
        self.db_dict, self.engine, self.Base, self.classes, self.inspector = initialization(db_config_path)
    
    def change_dict(self, table, field, key, BD):
        for index, row in table.iterrows():
            #если класс уже есть в словаре для данной строки, то расширяем словарь
            if field in BD[key]:
                k = max(BD.keys()) + 1
                new_dict = copy.deepcopy(BD[key])
                new_dict[field] = row
                BD.update({k : new_dict})
            else:
                BD[key][field] = row 
        return table, BD        
        
    def args(self, class_1, class_2):
        #восстанавливаем имена колонок, посредством которых связаны классы
        ref_col, child_col = [[elem[i][0] for elem in self.inspector.get_foreign_keys(class_1)
                if elem['referred_table'] == class_2][0] for i in 
                          ['constrained_columns', 'referred_columns']]
        #запрашиваем таблицы классов из базы
        parent_table, child_table = [self.Base.metadata.tables[i] for i in [class_1, class_2]]
        return ref_col, child_col, parent_table, child_table
    
    def query(self, class_1, class_2, BD, s):
        #собираем аргументы для формирования запроса к базе
        ref_col, child_col, parent_table, child_table = self.args(class_1, class_2)
        #делаем глубокую копию словаря, чтобы совершать итерации
        n_BD = copy.deepcopy(BD)
        
        if s == 'p':
            link, table, _class, field = [child_col, parent_table, class_1, class_2]
        else:
            link, table, _class, field = [ref_col, child_table, class_2, class_1]   
        for key, value in n_BD.items():
            #таблица класса, записанная в словаре
            row = value[field]
            type_ref_col = getattr(parent_table.c, ref_col).type.python_type
            #параметры, по которым будет осуществляться фильтрация
            filterargs = [getattr(parent_table.c, ref_col) == type_ref_col(row[link])]
            #запрос к двум связанным таблицам одновременно
            join_obj = parent_table.join(child_table,
                getattr(parent_table.c, ref_col) == getattr(child_table.c, child_col))
            #отбираем только те элементы таблицы искомого класса, которые удовлетворяют параметрам фильтрации
            sel_st = select(table.c).select_from(join_obj).where(or_(*filterargs)).distinct()
            res = self.engine.execute(sel_st)
            col_names = [elem['name'] for elem in self.inspector.get_columns(_class)]
            #сшиваем результат в pandas
            result = pd.DataFrame.from_dict(dict(zip(col_names, ik)) for i, ik in enumerate(res))
            #если одному исходном элементу известного класса, соответствует несколько эдементов искомого класса, то расширяем словарь
            if result.shape[0] != 1:
                result, BD = self.change_dict(result, _class, key, BD)
            #иначе, просто обновляем словарь
            else:
                BD[key].update({_class : row for index, row in result.iterrows()})   
        return BD
    
    def child_search(self, field, BD):
        #запрашиваем имена дочерних классов, проверяем, что их еще нет в словаре
        children = [elem['referred_table'] for elem in self.inspector.get_foreign_keys(field) 
                    if elem['referred_table'] not in BD[0]]
        for child in children:
            #восстанавливаем таблицу дочернего класса
            self.query(field, child, BD, s = 'c')
            #проверяем, есть ли у полученного класса дочерние
            self.child_search(child, BD)
            #проверяем, есть ли у полученного класса родительский
            self.parent_search(field, BD) 
        return BD
            
    def parent_search(self, field, BD):
        #проверяем, есть ли у заданного класса дочерние классы
        self.child_search(field, BD)
        #идем по всем существующим классам в базе
        for _class in self.classes:
            #запрашиваем все дочерние классы
            child_tables = [elem['referred_table'] for elem in self.inspector.get_foreign_keys(_class)]
            #если претендента на родительский класс нет в словаре и заданный класс является для него дочерним
            if (_class not in BD[0]) and (field in child_tables):
                #то делаем запрос родительской таблицы
                self.query(_class, field, BD, s = 'p')
                #проверяем, есть ли у полученного родительского класса родитель
                self.parent_search(_class, BD)              
        return BD
        
    def get_annotation(self, field = None, names = None):
        BD = defaultdict(dict)
        #рассматриваем два случая: поиск по классу и поиск по аттрибуту
        # if field in classes:
        #     key = field
        # else:
        for _class in self.classes:
            col_names = [elem['name'] for elem in self.inspector.get_columns(_class)]
            if field in col_names:
                key = _class
                break       
        #восстанавливаем таблицу запрашиваемого класса/класса, которому принадлежит запрашиваемый аттрибут, результат добавляем в словарь        
        field_table = pd.read_sql_table(key, self.engine)
        field_table = (field_table.loc[field_table[field].isin(names)]).reset_index(drop = True).astype(object) 
        if field_table.empty:
            raise Exception("There is no such values in database")
        BD.update({index : {key : row} for index, row in field_table.iterrows()})
        #ищем родителя данного класса    
        self.parent_search(key, BD)
        table = pd.concat([pd.concat([(_row.to_frame().T).reset_index(drop = True)
                                      for key, _row in value.items()], axis = 1) 
                            for i, value in BD.items()]).reset_index(drop = True)
        final_result = table.loc[:,~table.columns.duplicated()]
        fk = {elem['constrained_columns'][0] for _class in self.classes 
              for elem in self.inspector.get_foreign_keys(_class)}
        fk.add('id')
    
        return final_result.loc[:, ~final_result.columns.isin(fk)]
