# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 13:13:14 2021

@author: user94
"""
from pathlib import Path
import pandas as pd
import sys
sys.path.append(str(Path(__file__).resolve().parent.parent))

from sqlalchemy import select
from sqlalchemy import or_, and_ 
from sqlalchemy.sql.expression import bindparam  
from schema import initialization 

class DataUploader():
    def __init__(self):
        db_config_path = Path(__file__).resolve().parent / 'db_config.json'
        self.db_dict, self.engine, self.Base, self.classes, self.inspector = initialization(db_config_path)
        
    def check_nan(self, row, fields):
        if pd.isna(row[fields]).values.all():
            row[fields] = row[fields].fillna('Unknown')
        return row    
    
    def query(self, row, table, fields, col_name = None):
        types = {col : getattr(table.c, col).type.python_type for col in fields}
        try:
            filterargs = [getattr(table.c, col) == types[col](row[col]) for col in fields 
                          if not pd.isna(row[col])]
        except ValueError:
            filterargs = [getattr(table.c, col) == row[col] for col in fields 
                          if not pd.isna(row[col])]
        res = self.engine.execute(table.select().where(and_(*filterargs))).fetchone()
        if col_name:
            return getattr(res, col_name)
        else:
            if res:
                return 1
            else:
                return 0
     
    def id_col_search(self, field, table, fields = None):
        _class_table = self.Base.metadata.tables[field]
        if not fields:
            fields = table.columns.tolist()
        table.drop_duplicates(ignore_index = True, inplace = True)    
        table['q'] = table.apply(self.query, axis = 1, args = [_class_table, fields])
        (table[table['q'] == 0].loc[:, table.columns != 'q']).to_sql(field, 
                                self.engine, if_exists='append', index = False)
        
    def get_fields(self, _class, data):
        col_names = [elem['name'] for elem in self.inspector.get_columns(_class)]
        fields = [x for x in col_names if x in data.columns.tolist()]
        return fields
        
    def col_relationship(self, field, parent_table, parent_fields, data):
        for elem in self.inspector.get_foreign_keys(field):
            child_col = elem['referred_columns'][0]
            col_name = elem['constrained_columns'][0]
            child = elem['referred_table']
            child_table = self.Base.metadata.tables[child]
            child_fields = self.get_fields(child, data)
            table = data.loc[:, parent_fields + child_fields]
    
            table.apply(self.check_nan, axis = 1, args = [child_fields])
            table.apply(self.check_nan, axis = 1, args = [parent_fields])
            table.loc[:, col_name] = table.apply(self.query, axis = 1, 
                                                 args = [child_table, child_fields, child_col])
            table.drop_duplicates(ignore_index = True, inplace = True)
            filterargs = [getattr(parent_table.c, field) == bindparam('_' + field)
                          for field in parent_fields if not pd.isna(table[field]).values.any()]
    
            result = (table.rename(columns = 
                      {field: '_' + field for field in table.columns})).to_dict(orient='records')
            stmt = parent_table.update().where(and_(*filterargs)).\
                    values({
                        col_name: bindparam('_' + col_name)
                    })
            self.engine.execute(stmt, result)
    
    def add_table_relationship(self, field, data):
        child_cols = [elem['referred_columns'][0] for elem in 
                      self.inspector.get_foreign_keys(field)]
        children = [elem['referred_table'] for elem in 
                    self.inspector.get_foreign_keys(field)]
        col_names = [elem['constrained_columns'][0] for elem in 
                      self.inspector.get_foreign_keys(field)]
        children_tables = [self.Base.metadata.tables[child] for child in children]
        table = pd.DataFrame()
        for i, child in enumerate(children):
            child_fields = self.get_fields(child, data)
            child_df = data[child_fields].copy()
            child_df.apply(self.check_nan, axis = 1, args = [child_fields])
            table.loc[:, col_names[i]] = child_df.apply(self.query, axis = 1, 
                                                 args = [children_tables[i], child_df.columns, 
                                                         child_cols[i]])   
        self.id_col_search(field, table)
        
            
    def fill_table_info(self, _class, data):
        fields = self.get_fields(_class, data)
        subset = (data[fields]).drop_duplicates(ignore_index = True)
        if not subset.empty:
            subset.apply(self.check_nan, axis = 1, args = [subset.columns.tolist()])
            self.id_col_search(_class, subset, fields)
                
    def info_upload(self, data):
        #заполняем все таблицы БД информацией из переданных данных
        for _class in self.classes:
            #заполняем таблицу _class данными
            self.fill_table_info(_class, data)
            
        #устанавливаем связи между таблицами
        for _class in self.classes:
            #проверяем, связана ли таблица с другими таблицами
            if self.inspector.get_foreign_keys(_class):
                parent_table = self.Base.metadata.tables[_class]
                parent_fields = self.get_fields(_class, data)
                if not data[parent_fields].empty:
                    #таблицы связаны через доп колонку, устанавливаем отношения
                    self.col_relationship(_class, parent_table, parent_fields, data)
                else:
                    #таблицы связаны через доп таблицу, устанавливаем отношения
                    self.add_table_relationship(_class, data)
                    
    def add_data(self, data):
        data = data.astype(object)
        self.info_upload(data)   