# -*- coding: utf-8 -*-
"""
Created on Thu Dec 24 10:13:48 2020

@author: user94
"""
from pathlib import Path
import json
from sqlalchemy import create_engine, Column, Integer, Table, ForeignKey, String, Float, BigInteger
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.inspection import inspect
# from sqlalchemy_utils import get_referencing_foreign_keys
# from sqlalchemy.dialects.postgresql import ENUM
# from sqlalchemy.types import Enum
import pandas as pd

def initialization(db_config_path):
    # db_config_path = Path(__file__).resolve().parent / 'db_config.json'
    with db_config_path.open('r') as conf_file:
        db_dict = json.load(conf_file)
        
    engine = create_engine(
            'sqlite:///{path_to_blood_db}'.format(
                path_to_blood_db = db_dict['path'])
            )
    
    Base = declarative_base()
    Base.metadata.reflect(engine)
    classes = Base.metadata.tables.keys()
    inspector = inspect(engine)
    initial_table = pd.read_csv(db_dict['initial_table'], sep = '\t',  index_col= 'Columns')
    class_fields = initial_table['Parent'].unique()
    tables_exist = [engine.dialect.has_table(engine, table)
            for table in class_fields]
    if not all(tables_exist):
        class_creation(Base, initial_table, class_fields)
        Base.metadata.create_all(engine)
    return db_dict, engine, Base, classes, inspector

def class_creation(Base, initial_table, class_fields):    
    for class_field in class_fields:
        #определяем аттрибуты класса
        attributes = initial_table[(initial_table['Parent'] == class_field) &
                                  (pd.isna(initial_table['Link']))]
        if not attributes.empty:

            attr_dict = {'__tablename__' : class_field, 'id': Column(Integer, primary_key = True)}
            attr_dict.update({attribute : Column(globals()[attributes['Name_type'].tolist()[i]]) 
                              for i, attribute in enumerate(attributes.index.tolist())})
        #устанавливаем связи с дочерними классами
    
            children_classes = (initial_table[(initial_table['Parent'] == class_field) &
                                    (~pd.isna(initial_table['Link']))])
    
            attr_dict.update({col : Column(globals()[row['Name_type']], ForeignKey('.'.join([row['Link'], 'id'])))
                              for col, row in children_classes.iterrows()})
    
            parents = initial_table[(initial_table['Link'] == class_field)]['Parent']

            for field, parent in parents.items():
                if not initial_table[(initial_table['Parent'] == parent) &
                                  (pd.isna(initial_table['Link']))].empty:
                    attr_dict.update({parent.lower() : relationship(parent, back_populates = class_field.lower())})
            type(class_field, (Base,), attr_dict)
        else:
            children = initial_table[initial_table['Parent'] == class_field]
            args = [Column(field, globals()[row['Name_type']],
                                                ForeignKey('.'.join([row['Link'], 'id'])))
                                          for field, row in children.iterrows()]
            globals()[class_field] = Table(class_field, Base.metadata, *args)           